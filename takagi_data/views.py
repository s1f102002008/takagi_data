from django.shortcuts import render, redirect
"""検索機能用"""
from django.contrib import messages
from django.db.models import Q
from functools import reduce
from operator import and_
from takagi_data.models import Episode

# Create your views here.

def home(request):
    return render(request, 'takagi_data/home.html', {})

def takagi_database(request):
    episode = Episode.objects.all()
    return render(request, "takagi_data/takagi_database.html", {"episodes":episode})

def takagi_episode(request, episode_id):
    try:
        episode = Episode.objects.get(id=episode_id)
    except Episode.DoesNotExist:
        raise Http404("episode does not exist")

    return render(request, "takagi_data/takagi_episode.html", {"episode":episode})

def takagi_info(request):
    return render(request, "takagi_data/takagi_info.html")

def seach(request):
    #キーワード検索
    episode = Episode.objects.order_by('-id')
    keyword = request.GET.get('keyword')
    key_season = request.GET.get('key_season')
    key_epinum = request.GET.get('key_epinum')
    key_manga = request.GET.get('key_manga')
    key_situation = request.GET.get('key_situation')
    keywords = ""

    if key_manga:
        episode = episode.filter(
                 Q(g_num__iexact=key_manga)
               )
        keywords += (str(key_manga) + " ")

    if key_situation:
        episode = episode.filter(
                 Q(anime__iexact=key_situation)
               )
        keywords += (str(key_situation) + " ")

    if key_season:
        episode = episode.filter(
                 Q(anime_season__iexact=key_season)
               )
        keywords = str(keywords) + " アニメ" + str(key_season) + "期"

    if key_epinum:
        episode = episode.filter(
            Q(anime_num__iexact=key_epinum)
            )
        if key_epinum == '14':
            keywords += "OVA"
        elif key_epinum == '15':
            keywords += "劇場版"
        else:
            keywords += (str(key_epinum) + "話")

    if keyword:
        """ 除外リストを作成 """
        exclusion_list = set([' ', '　'])
        q_list = ''

        for i in keyword:
            """ 全角半角の空文字が含まれていたら無視 """
            if i in exclusion_list:
                pass
            else:
                q_list += i

        query = reduce(
                    and_, [Q(title__icontains=q) | Q(story__icontains=q) | 
                    Q(charactor__icontains=q) | 
                    Q(username__icontains=q) for q in q_list]
                )
        episode = episode.filter(query)
        keywords = keyword + " " + keywords
        messages.success(request, '「{}」の検索結果'.format(keyword))

    return render(request, 'takagi_data/takagi_database.html', {'episodes': episode , 'context':keywords})