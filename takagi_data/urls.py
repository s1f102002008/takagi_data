# chat/urls.py
from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path("takagi_database/",views.takagi_database,name="takagi_database"),
    path("takagi_episode/<int:episode_id>/",views.takagi_episode,name="takagi_episode"),
    path("takagi_info/",views.takagi_info,name="takagi_info"),
    path("seach/",views.seach,name="seach"),#キーワード検索
#    path('<str:room_name>/', views.room, name='room'),
]