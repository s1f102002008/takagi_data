from django.db import models
# Create your models here.

class Episode(models.Model):
    title = models.CharField(max_length=50,default="no text.")
    g_num = models.CharField(max_length=30,default="no text.")
    story = models.CharField(max_length=800,default="no text.")
    anime = models.CharField(max_length=10,default="no text.")
    anime_season = models.IntegerField(null=True, default=99)
    anime_num = models.IntegerField(null=True, default=99)
    charactor = models.CharField(max_length=150,default="no text.")
    username = models.CharField(max_length=150,default="運営")
# Create your models here.
